<?php

use App\Entity\Buyer;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    const ORDER_COUNT = 10;
    const ORDER_ITEM_COUNT = 8;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $products = Product::all();

        factory(Buyer::class, static::ORDER_COUNT)->create()
            ->each(function ($buyer) use ($products) {
                factory(Order::class, 1)->create(['buyer_id' => $buyer])
                    ->each(function ($order) use ($products){
                        $order->orderItems()->saveMany(
                            factory(OrderItem::class, static::ORDER_ITEM_COUNT)
                                ->make(['order_id'=>null])
                                ->each(function ($orderItem) use ($products) {
                                    $product = $products->shuffle()->shuffle()->first();
                                    $orderItem->product_id = $product->id;
                                    $price = $product->price;
                                    $discount = random_int(0, 75);
                                    $quantity = random_int(1, 50);
                                    $sum = OrderItem::calcSum($price, $quantity, $discount);
                                    $orderItem->price = $price;
                                    $orderItem->discount = $discount;
                                    $orderItem->quantity = $quantity;
                                    $orderItem->sum = $sum;
                                })
                        );
                    });
            });
    }
}
