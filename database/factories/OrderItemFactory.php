<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entity\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    $price = $faker->numberBetween(100, 10000);
    $discount = $faker->numberBetween(0, 75);
    $quantity = $faker->numberBetween(1, 50);
    $sum = OrderItem::calcSum($price, $quantity, $discount);
    return [
        'price' => $price,
        'quantity' => $quantity,
        'discount' => $discount,
        'sum' => $sum,
    ];
});
