<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entity\Buyer;
use Faker\Generator as Faker;

$factory->define(Buyer::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'country' => $faker->country,
        'city' => $faker->city,
        'address_line' => $faker->address,
        'phone' => $faker->phoneNumber,
    ];
});
