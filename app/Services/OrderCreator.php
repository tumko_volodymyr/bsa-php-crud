<?php


namespace App\Services;



use App\Entity\Order;
use App\Entity\OrderItem;
use App\Repository\BuyerRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Carbon\Carbon;

class OrderCreator
{

    private $orderRepository;
    private $productRepository;
    private $buyerRepository;

    public function __construct(
        OrderRepository $orderRepository,
        BuyerRepository $buyerRepository,
        ProductRepository $productRepository
    )
    {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->buyerRepository = $buyerRepository;
    }

    public function create(array  $data): Order
    {
        $buyerKey = 'buyerId';
        $productKey = 'productId';
        $productItemsKey = 'orderItems';

        if (!array_key_exists($buyerKey, $data)){
            throw new \InvalidArgumentException('buyerId is not specified');
        }

        if (!array_key_exists($productItemsKey, $data)){
            throw new \InvalidArgumentException('orderItems is not specified');
        }

        $orderItems = [];

        foreach ($data[$productItemsKey] as $productItem ){
            if (!array_key_exists($productKey, $productItem)){
                throw new \InvalidArgumentException('Order item is not valid');
            }
            $product = $this->productRepository->getById($productItem[$productKey]);
            $price = $product->price;
            $quantity = $productItem['productQty'];
            $discount = $productItem['productDiscount'];
            $sum = OrderItem::calcSum($price, $quantity, $discount);

            $orderItem = new OrderItem();
            $orderItem->product_id = $product->id;
            $orderItem->price = $price;
            $orderItem->discount = $discount;
            $orderItem->quantity = $quantity;
            $orderItem->sum = $sum;
            $orderItems[] = $orderItem;
        }

        $order = new Order();
        $buyer = $this->buyerRepository->getById($data[$buyerKey]);
        $order->buyer_id = $buyer->id;
        $order->date = new Carbon();
        $this->orderRepository->save($order);

        foreach ($orderItems as $orderItem ){
            $orderItem->order_id = $order->id;
            $order->orderItems()->save($orderItem);
        }

        return $order;
    }
}
