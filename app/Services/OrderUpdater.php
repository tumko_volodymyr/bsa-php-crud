<?php


namespace App\Services;



use App\Entity\Order;
use App\Entity\OrderItem;
use App\Repository\BuyerRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;

class OrderUpdater
{

    private $orderRepository;
    private $productRepository;
    private $buyerRepository;

    public function __construct(
        OrderRepository $orderRepository,
        BuyerRepository $buyerRepository,
        ProductRepository $productRepository
    )
    {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->buyerRepository = $buyerRepository;
    }

    public function create(array  $data, int $id): Order
    {
        $productKey = 'productId';
        $productItemsKey = 'orderItems';

        if (!array_key_exists($productItemsKey, $data)){
            throw new \InvalidArgumentException('orderItems is not specified');
        }

        $order = $this->orderRepository->getById($id);

        $orderItems = [];

        foreach ($data[$productItemsKey] as $productItem ){
            if (!array_key_exists($productKey, $productItem)){
                throw new \InvalidArgumentException('Order item is not valid');
            }
            $product = $this->productRepository->getById($productItem[$productKey]);
            $price = $product->price;
            $quantity = $productItem['productQty'];
            $discount = $productItem['productDiscount'];
            $sum = OrderItem::calcSum($price, $quantity, $discount);

            $orderItem = new OrderItem();
            $orderItem->product_id = $product->id;
            $orderItem->price = $price;
            $orderItem->discount = $discount;
            $orderItem->quantity = $quantity;
            $orderItem->sum = $sum;
            $orderItems[] = $orderItem;
        }

        $order->orderItems()->delete();

        foreach ($orderItems as $orderItem ){
            $orderItem->order_id = $order->id;
            $order->orderItems()->save($orderItem);
        }

        $this->orderRepository->save($order);

        return $this->orderRepository->getById($id);
    }
}
