<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     */

    public function toArray($request)
    {
        $orderItems = [];
        $orderSum = 0;
        $buyer = $this->buyer;
        foreach ($this->orderItems as $orderItem){
            $orderItems[] = [
                'productName' => $orderItem->product->name,
                'productQty' => $orderItem->quantity,
                'productPrice' => $orderItem->price/100,
                'productDiscount' => $orderItem->discount,
                'productSum' => $orderItem->sum/100,
            ];
            $orderSum += $orderItem->sum;
        }
        return [
            'orderId' => $this->id,
            'orderDate' => $this->date,
            'orderSum' => $orderSum/100,
            'orderItems' => $orderItems,
            'buyer' => [
                'buyerFullName' => sprintf('%s %s', $buyer->name, $buyer->surname),
                'buyerAddress' => sprintf('%s %s %s', $buyer->country, $buyer->city, $buyer->address_line),
                'buyerPhone' => $buyer->phone
            ]
        ];
    }
}
