<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Repository\OrderRepository;
use App\Services\OrderCreator;
use App\Services\OrderUpdater;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrderRepository $orderRepository)
    {
        $orders = $orderRepository->findAll();
        return $orders->map(function ($order){
            return new OrderResource($order);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, OrderCreator $creator)
    {
        $data = Input::all();
        try{
            $order = $creator->create($data);
            return new OrderResource($order);

        }catch (\InvalidArgumentException | ModelNotFoundException $exception ){
            return new Response([
                'result' => 'fail',
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, OrderRepository $orderRepository)
    {
        $order = $orderRepository->getById($id);
        return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, OrderUpdater $orderUpdater)
    {
        $data = Input::all();
        try{
            $order = $orderUpdater->create($data, $id);
            return new OrderResource($order);

        }catch (\InvalidArgumentException | ModelNotFoundException $exception ){
            return new Response([
                'result' => 'fail',
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, OrderRepository $orderRepository)
    {
        $result = $orderRepository->deleteById($id);
        return ['result' => $result ? 'success' : 'fail'];
    }
}
