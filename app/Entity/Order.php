<?php

declare(strict_types=1);

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App\Entity
 * @property int $id
 * @property OrderItem $orderItems
 * @property Buyer $buyer
 * @property Carbon $date
 * @property Carbon $created_at
 */
class Order extends Model
{
    protected $with = ['buyer', 'orderItems'];

    public function buyer(){
        return $this->belongsTo(Buyer::class);
    }

    public function orderItems(){
        return $this->hasMany(OrderItem::class);
    }
}
