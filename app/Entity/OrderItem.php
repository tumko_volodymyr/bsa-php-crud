<?php

declare(strict_types=1);

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderItem
 * @package App\Entity
 * @property int $id
 * @property Order $order
 * @property Product $product
 * @property int $price
 * @property int $quantity
 * @property int $discount
 * @property int $sum
 * @property Carbon $created_at
 */
class OrderItem extends Model
{
    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public static function calcSum($price, $quantity, $discount): int
    {
        return (int) round($price * $quantity * ((100 - $discount)/100));
    }
}
