<?php

declare(strict_types=1);

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Buyer
 * @package App\Entity
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $country
 * @property string $city
 * @property string $address_line
 * @property string $phone
 * @property Order[] $orders
 * @property Carbon $created_at
 */
class Buyer extends Model
{
    public function orders(){
        return $this->hasMany(Order::class);
    }
}
